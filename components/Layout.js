import Head from "next/head";
import styled from "styled-components";

const Main = styled.main`
 display: flex;
 height: 100vh;
 max-height: 100vh;
 min-height: 100vh;
`

function Layout({ children }) {
  return (
    <>
      <Head>
        <title>Yoe NextJS Chat</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Main>{children}</Main>
    </>
  );
}

export default Layout;
