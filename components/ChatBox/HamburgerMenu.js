import PropTypes from "prop-types";
import styled from "styled-components";

const StyledHamburgerMenu = styled.div`
  display: none;
  @media (min-width: 615px) and (max-width: 769px) {
    width: 32px;
    height: 24px;
    padding: 4px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    position: absolute;
    right: 24px;
    & div {
      background-color: #5c91fc;
      height: 4px;
      width: 100%;
    }
  }
`;

const HamburgerMenu = () => (
  <StyledHamburgerMenu>
    <div />
    <div />
    <div />
  </StyledHamburgerMenu>
);

HamburgerMenu.propTypes = {
  openMenu: PropTypes.bool,
};

HamburgerMenu.defaultProps = {
  openMenu: true,
};

export default HamburgerMenu;
