import { useState, useRef } from "react";
import styled, { css } from "styled-components";
import HamburgerMenu from "./HamburgerMenu";

const StyledChatBox = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  max-height: 100%;
`;

const commonStyle = css`
  height: 68px;
  width: 100%;
  display: flex;
  align-items: center;
  background-color: #f9f9f9;
`;

const ChatBoxHeader = styled.div`
  padding: 12px;
  border-bottom: 1px solid #dfe4ea;
  font-size: 22px;
  & img {
    border-radius: 50%;
    margin: 0 12px;
  }
  ${() => commonStyle}
`;

const ChatBoxFooter = styled.div`
  padding: 8px;
  border-top: 1px solid #dfe4ea;
  align-self: flex-end;
  align-items: stretch;
  display: flex;
  & form {
    display: flex;
    align-items: stretch;
    flex-grow: 1;
    & input,
    & button {
      border-radius: 6px;
      min-height: 50px;
    }
  }
  ${() => commonStyle}
`;

const FooterInput = styled.input`
  flex-grow: 1;
  border: 2px solid #dfe4ea;
  background-color: #eeeeee;
`;

const FooterButton = styled.button`
  color: white;
  background-color: #7e72f2;
  border: none;
  margin-left: 8px;
  padding: 0 24px;
`;

const ChatBody = styled.div`
  width: 100%;
  height: 100%;
  max-height: 100%;
  align-self: stretch;
  flex-grow: 1;
  background-image: url("/images/BACKGROUND.png");
  background-size: cover;
  background-position: center;
  overflow-x: hidden;
  overflow-y: scroll;
  border-right: 4px solid #f2f1f7;
  &::-webkit-scrollbar-track {
    background: #e5e5e5;
    border: 4px solid #f2f1f7;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #7e72f2;
  }
`;

const ChatBox = ({ userName, avatar }) => {
  const [messages, setMessages] = useState([]);
  const footerInputRef = useRef(null);

  const sendMessage = (e) => {
    e.preventDefault();
    const text = footerInputRef.current.value.trim();

    if (text) {
      const msgCreatedDate = Date.now();

      setMessages([
        ...messages,
        {
          id: msgCreatedDate,
          created_date: msgCreatedDate,
          text,
        },
      ]);
    }

    // limpiando
    footerInputRef.current.value = "";
  };

  const Message = styled.div`
    padding: 8px;
    display: flex;
    ${({ isUserMessage }) =>
      isUserMessage &&
      css`
        flex-direction: row-reverse;
      `}
    & img {
      border-radius: 50%;
    }
    & p {
      max-width: 66.6%;
      overflow-wrap: break-word;
      color: white;
      margin: 0 16px;
      padding: 8px 20px;
      background-color: ${({ theme, isUserMessage }) =>
        isUserMessage ? theme.colors.secondary : theme.colors.primary.medium};
      border-radius: 6px;
    }
  `;

  return (
    <StyledChatBox>
      <ChatBoxHeader>
        <img src={avatar} width={40} height={40} />
        <p>{userName}</p>
        <HamburgerMenu />
      </ChatBoxHeader>
      <ChatBody>
        {messages.map(({ id, text }) => (
          <Message key={id}>
            <img src="/images/MOCK_USER_AVATAR.jpg" width={40} height={40} />
            <p>{text}</p>
          </Message>
        ))}
      </ChatBody>
      <ChatBoxFooter>
        <form onSubmit={sendMessage}>
          <FooterInput ref={footerInputRef} />
          <FooterButton type="submit" placeholder="Type your message...">
            SEND
          </FooterButton>
        </form>
      </ChatBoxFooter>
    </StyledChatBox>
  );
};

export default ChatBox;
