import styled from "styled-components";
import PropTypes from "prop-types";
import Image from "next/image"

const StyledUserInfo = styled.div`
  padding: 12px 20px;
  display: flex;
  flex-direction: column;
  & img {
    border-radius: 50%;
  }
  & span {
    color: #7E72F2;
    font-size: 22px;
    padding-top: 4px;
  }
  & p {
    margin: 0;
    color: white;
  }
`

const UserInfo = ({ name, email, avatar }) => (
  <StyledUserInfo>
    <Image src={avatar} alt={name} width={80} height={80} layout="fixed" priority/>
    <span>{name}</span>
    <p>{email}</p>
  </StyledUserInfo>
);

UserInfo.propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired
}

export default UserInfo