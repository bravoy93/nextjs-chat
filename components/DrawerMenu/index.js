import { useState } from "react";
import styled, {css} from "styled-components";
import PropTypes from "prop-types";
import UserInfo from "./UserInfo";
import ChatContact from "./ChatContact";

const DrawerHeader = styled.div`
  width: 100%;
  border-bottom: 1px solid #0b0f2d;
`;

const DrawerContainer = styled.div`
  padding: 4px;
  height: 100vh;
  width: 324px;
  min-width: 324px;
  top: 0;
  left: 0;
  background-color: #0f1642;
  overflow: hidden;
  display: flex;
  flex-direction: column;  
  transition: all 300ms;
  ${({ openMenu }) =>
    !openMenu &&
    css`
      transform: translateX(-324px);
      @media (max-width: 614px) {
        transform: translateX(-100%);
    `}
  @media (max-width: 614px) {
    position: fixed;
    z-index: 1;
    width: 100%;
  }
`;

const ContactsList = styled.ul`
  margin: 0;
  padding: 0 0 24px 0;
  list-style: none;
  margin-top: 16px;
  overflow-y: scroll;
  &::-webkit-scrollbar-track {
    background: #0b0f2d;
  }
  &::-webkit-scrollbar-thumb {
    background: rgba(126, 114, 242, 0.3);
  }
`;

const DrawerMenu = ({ items }) => {
  const [openMenu, setOpenMenu] = useState(true);

  return (
    <DrawerContainer openMenu={openMenu}>
      <DrawerHeader>
        <UserInfo
          name="Yoe Bravo"
          email="bravoy93@gmail.com"
          avatar="/images/MOCK_USER_AVATAR.jpg"
        />
      </DrawerHeader>
      <ContactsList>
        {items.map((p) => (
          <ChatContact key={p.id} {...p} />
        ))}
      </ContactsList>
    </DrawerContainer>
  );
}

DrawerMenu.propTypes = {
  items: PropTypes.array,
};

export default DrawerMenu;
