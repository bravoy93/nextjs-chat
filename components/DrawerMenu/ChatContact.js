import styled, { css } from "styled-components";
import PropTypes from "prop-types";

const StyledChatContact = styled.li`  
margin-right: 4px;
padding: 12px 8px;
font-size: 18px;
color: white;
cursor: pointer;  
display: flex;
align-items: center;
border-radius: 4px;    
& img {
  border-radius: 50%;
}
& .contact-info {
  display: flex;
  flex-wrap: wrap;
  padding-left: 12px;      
  & p {
    font-weight: 200;
    margin: 0;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    width: 94%;
  }
}
&:hover,
&:active,
&:focus {
  background-color: rgba(126, 114, 242, 0.3);
};
${({ selected }) =>
  selected &&
  css`
    background-color: rgba(126, 114, 242, 0.3);
  `}  
`;

const ChatContact = ({ name, email, avatar, selected }) => {
  return (
    <StyledChatContact selected={selected}>
      <img src={avatar} alt={name} width={40} height={40} />
      <div className="contact-info">
        <span>{name}</span>
        <p>{email}</p>
      </div>
    </StyledChatContact>
  );
};

ChatContact.propTypes = {
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  selected: PropTypes.bool,
};

export default ChatContact;
