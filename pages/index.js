import { useState } from "react";
import { Layout, DrawerMenu, ChatBox } from "../components";
import {useMockChatContacts} from "../hooks/useMockChatContacts"

const Home = () => {
  const MOCK_CHAT_CONTACTS = useMockChatContacts()
  const [chatContactsList, setChatContactsList] = useState(MOCK_CHAT_CONTACTS)

  return (
    <Layout>
      <DrawerMenu items={chatContactsList} />
      <ChatBox userName="Claudia Gozález" avatar="/images/MOCK_USER_AVATAR.jpg" />
    </Layout>
  );
};

export default Home;
