const MOCK_CHAT_CONTACTS = [
  {
    id: 1,
    name: "Carlos",
    password: "",
    email: "carlos@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
  {
    id: 2,
    name: "Frank",
    password: "",
    email: "frank@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
  {
    id: 3,
    name: "Alicia",
    password: "",
    email: "alicia@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
  {
    id: 5,
    name: "Pedrito Marianao",
    password: "",
    email: "pedritomarianao@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
  {
    id: 6,
    name: "Pedrito Hermana",
    password: "",
    email: "pedritohermana@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
  {
    id: 7,
    name: "Maykel",
    password: "",
    email: "maykel@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
  {
    id: 8,
    name: "Estela",
    password: "",
    email: "estela@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
  {
    id: 9,
    name: "Felo",
    password: "",
    email: "felo@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
  {
    id: 10,
    name: "Elena la Cocinera",
    password: "",
    email: "elenalacocinera@gmail.com",
    avatar: "/images/MOCK_USER_AVATAR.jpg",
  },
];

const useMockChatContacts = () => MOCK_CHAT_CONTACTS;

export { useMockChatContacts };
